{
  name = "lyk";

  compiler = "ghc884";

  shellProvidedPackages = pkgs: [
    pkgs.haskellPackages.implicit-hie
    pkgs.entr
    pkgs.niv
    pkgs.gnumake
  ];

  getMainPackage = haskellPackages: haskellPackages.equilibrium-test-binary;
}
