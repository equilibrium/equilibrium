#!/usr/bin/env sh

clear
echo "[1/2] Generating files..."
nix-shell --pure --run "make generated-noshell" nix/setupshell.nix

clear
echo "[2/2] Should I setup git hooks for this repository?"
echo "  WARNING: this could delete hooks you have set up yourself!"
while true; do
  read -p "  [y/n] " yn
  case $yn in
    [Yy]* ) echo "Copying pre-commit hook...";\
      cp hooks/pre-commit .git/hooks/pre-commit;\
      break;;
    [Nn]* ) echo "Not setting up hooks.";\
      echo "Please ensure you're following the project's conventions.";\
      break;;
    * ) echo "Please answer yes or no.";;
  esac
done

clear
echo "🚀🚀🚀 All done! 🚀🚀🚀
You're now ready to start working.

To enter a nix-shell with all required dependencies:
  $ nix-shell

From inside the nix-shell, you can:

  Generate all required files to build the project and the nix-shell:
    $ make generate

  Build the project:
    $ make build

  Run all tests:
    $ make test

  Generate files, test, and build the project from a clean state:
    $ make all

You can find all of these in the README too, for later reference.
"

echo "Should I put you in a nix-shell?"
while true; do
  read -p "  [y/n] " yn
  case $yn in
    [Yy]* ) echo "Entering nix-shell..."; nix-shell; echo "Bye!"; break;;
    [Nn]* ) echo "You can manually enter the nix-shell anytime by running:
  $ nix-shell"; break;;
    * ) echo "Please answer yes or no.";;
  esac
done

