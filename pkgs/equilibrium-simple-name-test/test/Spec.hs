import           Test.Hspec
import           Test.QuickCheck

import qualified Test.Equilibrium.Logic.SimpleName

main :: IO ()
main = hspec $ do
  Test.Equilibrium.Logic.SimpleName.tests
