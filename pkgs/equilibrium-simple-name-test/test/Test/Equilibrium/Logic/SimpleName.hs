{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE TemplateHaskell       #-}

module Test.Equilibrium.Logic.SimpleName where

import           Data.Text                      ( Text )
import           ECS
import           Equilibrium
import           Equilibrium.Logic.SimpleId.Types
import           Equilibrium.Logic.SimpleName
import           Equilibrium.Logic.SimpleName.Types
import           Test.Hspec
import           Test.QuickCheck

tests = do
  describe "deleteTodoSystem" $ do
    it "should delete a todo item" $ do
      -- given
      let id = SimpleId 1
      let todos =
            [ entity [Component_ $ SimpleId 0, Component_ $ SimpleName "jey"]
            , entity [Component_ id, Component_ $ SimpleName "j0"]
            ]

      -- when
      changedTodos <- applySystem (deleteTodoSystem id) todos

      -- then
      length changedTodos `shouldBe` 1
      changedTodos
        `shouldContain` [ entity
                            [ Component_ $ SimpleId 0
                            , Component_ $ SimpleName "jey"
                            ]
                        ]

  describe "renameSystem" $ do
    it "should rename a todo item" $ do
      -- given
      let id      = SimpleId 2
      let newName = "lol"
      let todos =
            [ entity [Component_ $ SimpleId 0, Component_ $ SimpleName "jey"]
            , entity [Component_ $ SimpleId 1, Component_ $ SimpleName "j0"]
            , entity [Component_ id, Component_ $ SimpleName "kek"]
            ]

      -- when
      changedTodos <- applySystem (renameSystem id newName) todos

      -- then
      length changedTodos `shouldBe` 3
      changedTodos
        `shouldContain` [ entity
                          [ Component_ $ SimpleId 0
                          , Component_ $ SimpleName "jey"
                          ]
                        , entity
                          [ Component_ $ SimpleId 1
                          , Component_ $ SimpleName "j0"
                          ]
                        , entity [Component_ id, Component_ $ SimpleName "lol"]
                        ]
