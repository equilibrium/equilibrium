{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE KindSignatures #-}
module Equilibrium.Logic.SimpleInterface where

import           Control.Monad.IO.Class         ( MonadIO )
import           Data.List                      ( intercalate
                                                , sortOn
                                                )
import           Data.Text                      ( unpack )
import           ECS

import           Equilibrium.Logic.SimpleId.Types
import           Equilibrium.Logic.SimpleName.Types
import           Equilibrium.Logic.SimpleState.Types

-- TODOREF
-- simpleListTasksSystem
--   :: forall w
--    . (Has w IO SimpleId, Has w IO SimpleState, Has w IO SimpleName)
--   => SystemT w IO String
-- simpleListTasksSystem = do
--   todosList <- cfold
--     (\ls (SimpleId id, SimpleState state, SimpleName name) ->
--       (id, (if state then "DONE" else "TODO") ++ " " ++ (unpack name)) : ls
--     )
--     []
-- 
--   let sortedTodos = sortOn (fst) todosList
--   return $ intercalate "\n" $ map (\(id, s) -> show id ++ ": " ++ s) sortedTodos

