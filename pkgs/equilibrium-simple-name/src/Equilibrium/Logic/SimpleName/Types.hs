{-# LANGUAGE OverloadedStrings #-}
module Equilibrium.Logic.SimpleName.Types where

import           Data.Aeson
import           Data.Text
import           ECS

newtype SimpleName = SimpleName Text
  deriving (Show)

instance Component SimpleName where
  identifier = "Name"
  serialize (SimpleName name) = String name
  deserialize (String name) = Just $ SimpleName name
