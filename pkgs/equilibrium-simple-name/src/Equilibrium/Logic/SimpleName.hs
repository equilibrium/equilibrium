{-# LANGUAGE RankNTypes          #-}
{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE TypeApplications    #-}
{-# LANGUAGE ScopedTypeVariables #-}
module Equilibrium.Logic.SimpleName where

import           Data.Text                      ( pack )
import           ECS
import           Equilibrium.Logic.SimpleName.Types

addTodoSystem :: Component_ -> String -> LoadingSystem
addTodoSystem id name =
  return [entity [id, Component_ $ SimpleName $ pack name]]

deleteTodoSystem
  :: forall i e
   . (HasComponent e i, Component i, Eq i, RetrievesComponents e)
  => i
  -> UpdateSystem e e
deleteTodoSystem targetId entity =
  if get @i entity == targetId then Delete else Keep

renameSystem
  :: forall i e e'
   . ( HasComponent e i
     , HasComponent e' i
     , HasComponent e' SimpleName
     , Component i
     , Eq i
     , RetrievesComponents e
     , SavesComponents e e'
     )
  => i
  -> String
  -> UpdateSystem e e'
renameSystem targetId newName entity = if get @i entity == targetId
  then Update $ set (SimpleName $ pack newName) entity
  else Keep

