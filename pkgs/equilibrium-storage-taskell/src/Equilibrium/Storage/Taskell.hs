{-# LANGUAGE RankNTypes          #-}
{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE TypeFamilies        #-}
{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE ScopedTypeVariables #-}
module Equilibrium.Storage.Taskell where

import           Data.Foldable                  ( toList )
import           Data.Function                  ( (&) )
import qualified Data.Text                     as T
import           ECS

-- import           Taskell.IO
import           ClassyPrelude                  ( runReaderT )
import           Data.Time.Zones                ( loadLocalTZ )
-- import           Taskell.IO.Config              ( setup )

import           Equilibrium.Logic.SimpleName.Types
-- import           Equilibrium.Logic.TaskellState.Types
-- import           Taskell.Data.List              ( _tasks
                                                -- , _title
                                                -- )
-- import           Taskell.Data.Lists             ( Lists )
-- import           Taskell.Data.Task              ( _name )
-- TODOREF
-- 
-- (>.>) :: (a -> b) -> (b -> c) -> a -> c
-- (>.>) f g = g . f
-- 
-- loadingIOSystem :: IO (Either T.Text Lists)
-- loadingIOSystem = do
--   config   <- setup
--   timezone <- loadLocalTZ
--   result   <- runReaderT (readData "taskell.md") (IOInfo timezone config)
--   return result
-- 
-- loadingParseSystem
--   :: forall w
--    . ( Has w IO EntityCounter
--      , Has w IO TaskellColumnState
--      , Has w IO SimpleName
--      )
--   => Either T.Text Lists
--   -> System w ()
-- loadingParseSystem result = do
--   case result of
--     Right columns ->
--       columns
--         & toList
--         & map
--             ((\taskellList ->
--                let state = _title taskellList
--                in
--                  taskellList
--                  & _tasks
--                  & toList
--                  & map
--                      (   _name
--                      >.> \n ->
--                            newEntity
--                              ( SimpleName n
--                              , TaskellColumnState $ T.unpack state
--                              )
--                      )
--                  & sequence_
--              )
--             )
--         & sequence_
--     Left _ -> return ()
-- 
-- 
-- loadingSystem
--   :: forall w
--    . ( Has w IO TaskellColumnState
--      , Has w IO SimpleName
--      , Has w IO EntityCounter
--      )
--   => System w (IO ())
-- loadingSystem = loadingSystem' loadingIOSystem loadingParseSystem
-- 
-- loadingSystem'
--   :: forall a w
--    . ( Has w IO TaskellColumnState
--      , Has w IO SimpleName
--      , Has w IO EntityCounter
--      )
--   => IO a
--   -> (a -> System w ())
--   -> System w (IO ())
-- loadingSystem' loadingIOSystem loadingParseSystem = do
--   fileContents <- liftIO $ loadingIOSystem
--   loadingParseSystem fileContents
--   return $ liftIO $ return ()
-- 
