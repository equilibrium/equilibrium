{-# LANGUAGE OverloadedStrings #-}

module Equilibrium.Logic.SimpleId.Types where

import           Data.Aeson
import           Data.Maybe                     ( fromJust )
import           Data.Scientific                ( toBoundedInteger )
import           ECS

newtype SimpleId = SimpleId Int
  deriving (Show, Eq)

instance Component SimpleId where
  identifier = "Id"
  serialize (SimpleId id) = Number $ fromIntegral id
  deserialize (Number id) = SimpleId <$> (toBoundedInteger id :: Maybe Int)
