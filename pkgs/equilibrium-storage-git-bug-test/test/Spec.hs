import           Test.Hspec
import           Test.QuickCheck

import qualified Test.Equilibrium.Storage.GitBug
import qualified Test.Equilibrium.Storage.GitBug.CLI

main :: IO ()
main = hspec $ do
  Test.Equilibrium.Storage.GitBug.CLI.tests
  Test.Equilibrium.Storage.GitBug.tests
