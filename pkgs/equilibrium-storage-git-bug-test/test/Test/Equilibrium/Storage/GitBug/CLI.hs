module Test.Equilibrium.Storage.GitBug.CLI where

import           Test.Hspec
import           Test.QuickCheck

import           Data.Maybe                     ( fromJust )
import           System.Exit                    ( ExitCode
                                                  ( ExitFailure
                                                  , ExitSuccess
                                                  )
                                                )

import           Equilibrium.Storage.GitBug.CLI
import           Equilibrium.Storage.GitBug.Internal
import           Equilibrium.Storage.GitBug.Types

tests = do
  describe "lsId" $ do
    it "should correctly parse output" $ do
      -- given
      let fstId = "bce7125199425b7438d333aa93b01c33008cc96c"
          sndId = "fe08f03cf563ea06472bd5270a65e5ace4401af7"
          trdId = "68a89229c2ebc47769863679076ccb3edbca82ab"

          proc :: IO (ExitCode, String, String)
          proc = return (ExitSuccess, unlines [fstId, sndId, trdId], "")

      -- when
      ids <- lsId' proc

      -- then
      let ids' = map gitBugIdAsString ids
      ids' `shouldContain` [fstId]
      ids' `shouldContain` [sndId]
      ids' `shouldContain` [trdId]

    it "should handle errors gracefully" $ do
      -- given
      let
        code = 1

        proc :: IO (ExitCode, String, String)
        proc =
          return (ExitFailure code, unlines ["0", "1", "2"], "Error message")

      -- when
      ids <- lsId' proc

      -- then
      length ids `shouldBe` 0

  describe "title" $ do
    it "should return title without newline" $ do
      -- given
      let idString      = "jfieokfmd7"

          expectedTitle = "This is the title"

          proc :: String -> IO (ExitCode, String, String)
          proc id = if id == idString
            then return (ExitSuccess, unlines [expectedTitle], "")
            else return (ExitFailure 1, "", "")

          title = title' proc

          id    = gitBugIdFromString idString

      -- when
      mTitle <- title id

      -- then
      mTitle `shouldBe` Just expectedTitle

    it "should handle errors correctly" $ do
      -- given
      let code = 1

          proc :: String -> IO (ExitCode, String, String)
          proc _ = return (ExitFailure code, "", "Some error message")

          title = title' proc

          id    = gitBugIdFromString "123"

      -- when
      mTitle <- title id

      -- then
      mTitle `shouldBe` Nothing

  describe "status" $ do
    it "should handle linebreak of status" $ do
      -- given
      let idString = "548wuejfd"

          proc :: String -> IO (ExitCode, String, String)
          proc id = if id == idString
            then return (ExitSuccess, unlines ["closed"], "")
            else return (ExitFailure 1, "", "")

          status = status' proc

          id     = gitBugIdFromString idString

      -- when
      mStatus <- status id

      -- then
      mStatus `shouldBe` Just True

    it "should handle errors correctly" $ do
      -- given
      let code = 1

          proc :: String -> IO (ExitCode, String, String)
          proc _ = return (ExitFailure code, "", "Weird error message")

          status = status' proc

          id     = gitBugIdFromString "124"

      -- when
      mStatus <- status id

      -- then
      mStatus `shouldBe` Nothing
