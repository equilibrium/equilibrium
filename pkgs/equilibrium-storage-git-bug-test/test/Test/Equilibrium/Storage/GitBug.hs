{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TemplateHaskell       #-}
module Test.Equilibrium.Storage.GitBug where

import           Test.Hspec
import           Test.QuickCheck

import           Data.Maybe                     ( fromJust )
import           Data.Text                      ( Text )
import           System.Exit                    ( ExitCode(ExitSuccess) )

import           ECS

import           Equilibrium
import           Equilibrium.Logic.SimpleName.Types
import           Equilibrium.Logic.SimpleState.Types

import           Equilibrium.Storage.GitBug.Internal
import           Equilibrium.Storage.GitBug.CLI
import           Equilibrium.Storage.GitBug.Types
import           Equilibrium.Storage.GitBug.Composer

tests = do
  describe "loadingSystemComposer with mocks" $ do
    it "should load three tasks" $ do
      -- given
      let
        fstId = "bce7125199425b7438d333aa93b01c33008cc96c"
        sndId = "fe08f03cf563ea06472bd5270a65e5ace4401af7"
        trdId = "68a89229c2ebc47769863679076ccb3edbca82ab"

        lsIdProc :: IO (ExitCode, String, String)
        lsIdProc = return $ (ExitSuccess, unlines [fstId, sndId, trdId], "")

        lsId :: LsIdCLI
        lsId = lsId' lsIdProc

        title :: TitleCLI
        title id
          | gitBugIdAsString id == fstId = return $ Just "first git bug issue"
          | gitBugIdAsString id == sndId = return $ Just "second git bug issue"
          | gitBugIdAsString id == trdId = return $ Just "third git bug issue"
          | otherwise                    = return Nothing

        status :: StatusCLI
        status id | gitBugIdAsString id == fstId = return $ Just False
                  | gitBugIdAsString id == sndId = return $ Just False
                  | gitBugIdAsString id == trdId = return $ Just True
                  | otherwise                    = return Nothing

      -- when
      tasks <- gitBugLoadingSystemComposer lsId status title

      -- then
      length tasks `shouldBe` 3
      tasks `shouldContain` [entity [Component_ $ gitBugIdFromString fstId, Component_ $ SimpleState False, Component_ $ SimpleName "first git bug issue"]]
      tasks `shouldContain` [entity [Component_ $ gitBugIdFromString sndId, Component_ $ SimpleState False, Component_ $ SimpleName "second git bug issue"]]
      tasks `shouldContain` [entity [Component_ $ gitBugIdFromString trdId, Component_ $ SimpleState True , Component_ $ SimpleName "third git bug issue"]]

  describe "savingSystem" $ do
    it "TODO" $ do
      1 `shouldBe` 1
