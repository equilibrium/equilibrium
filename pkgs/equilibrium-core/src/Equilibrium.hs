{-# LANGUAGE TemplateHaskell #-}
module Equilibrium where

import           Control.Monad                  ( forM )
import           ECS
import           Language.Haskell.TH.Syntax     ( Dec(TySynD)
                                                , Name
                                                , Q
                                                , TyVarBndr(PlainTV)
                                                , Type(ConT, TupleT)
                                                , mkName
                                                , newName
                                                )

-- TODOREF
-- updateSystem
--   :: System w (IO ()) -> System w () -> System w (IO ()) -> System w ()
-- updateSystem loadingSystem logicSystem savingSystem = do
--   loadingSystem
--   logicSystem
--   savingSystem
--   return ()
-- 
-- getInfoSystem :: System w (IO ()) -> System w a -> System w a
-- getInfoSystem loadingSystem extractInfoSystem = do
--   loadingSystem
--   extractInfoSystem
-- 
-- genName :: String -> Q Name
-- genName s = mkName . show <$> newName s
-- 
-- -- | Creates an environment ("world") called "TaskManager" with the given components
-- -- | Generates the initTaskManager function
-- generateTaskManagerWithComponents :: [Name] -> Q [Dec]
-- generateTaskManagerWithComponents = generateTypes "TaskManager"
-- 
-- generateTypes :: String -> [Name] -> Q [Dec]
-- generateTypes name cTypes = do
--   cTypesNames <- forM cTypes $ \t -> do
--     rec <- genName "rec"
--     return (ConT t, rec)
-- 
--   let cNames = map snd cTypesNames
-- 
--       allCsName :: Name
--       allCsName = mkName "AllComponents"
-- 
--       tyVarBndrs :: [TyVarBndr]
--       tyVarBndrs = map (\n -> PlainTV n) cNames
-- 
--       type' :: Type
--       type' = TupleT $ length cTypes
-- 
--       allComponentsDec :: Dec
--       allComponentsDec = TySynD allCsName tyVarBndrs type'
-- 
--   worldDecs <- makeWorld name cTypes
--   return $ allComponentsDec : worldDecs

