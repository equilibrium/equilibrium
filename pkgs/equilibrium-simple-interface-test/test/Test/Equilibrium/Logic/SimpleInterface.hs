{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TemplateHaskell       #-}
module Test.Equilibrium.Logic.SimpleInterface where

import           Test.Hspec
import           Test.QuickCheck

import           Data.List                      ( intercalate )
import           Data.Text                      ( Text
                                                , pack
                                                )
import           ECS

import           Equilibrium
import           Equilibrium.Logic.SimpleId.Types
import           Equilibrium.Logic.SimpleInterface
import           Equilibrium.Logic.SimpleName.Types
import           Equilibrium.Logic.SimpleState.Types

-- TODOREF
-- generateTaskManagerWithComponents [ ''SimpleId, ''SimpleState, ''SimpleName ]
-- 
-- tests = do
--   describe "" $ do
--     it "" $ do
--       -- given
--       taskMgr <- initTaskManager
-- 
--       let todos = [(0, True, "my todo"), (5, False, "another todo")]
-- 
--       mapM
--         (\(id, isDone, name) -> runSystem
--           (newEntity (SimpleId id, SimpleState isDone, SimpleName name))
--           taskMgr
--         )
--         todos
-- 
--       -- when
--       output <- runSystem (simpleListTasksSystem) taskMgr
-- 
--       -- then
--       output
--         `shouldBe` (intercalate "\n" ["0: DONE my todo", "5: TODO another todo"]
--                    )

