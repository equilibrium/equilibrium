{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TemplateHaskell       #-}
module Test.Equilibrium.Logic.SimpleState where

import           Test.Hspec
import           Test.QuickCheck

import           ECS

import           Equilibrium
import           Equilibrium.Logic.SimpleId.Types
import           Equilibrium.Logic.SimpleName.Types
import           Equilibrium.Logic.SimpleState
import           Equilibrium.Logic.SimpleState.Types

tests = do
  describe "toggleSystem" $ do
    it "should toggle task to done" $ do
      -- given
      let id = SimpleId 0
      let todos =
            [ entity
              [ Component_ id
              , Component_ $ SimpleState False
              ]
            ]

      -- when
      toggledTodos <- applySystem (toggleSystem id) todos

      -- then
      length toggledTodos `shouldBe` 1
      toggledTodos `shouldContain`
        [ entity
          [ Component_ id
          , Component_ $ SimpleState True
          ]
        ]

    it "should toggle task to not done" $ do
      -- given
      let id = SimpleId 3
      let todos =
            [ entity
              [ Component_ id
              , Component_ $ SimpleState True
              ]
            ]

      -- when
      toggledTodos <- applySystem (toggleSystem id) todos

      -- then
      length toggledTodos `shouldBe` 1
      toggledTodos `shouldContain`
        [ entity
          [ Component_ id
          , Component_ $ SimpleState False
          ]
        ]
