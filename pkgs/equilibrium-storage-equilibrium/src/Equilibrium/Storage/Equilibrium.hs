{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE RankNTypes          #-}
{-# LANGUAGE TypeFamilies        #-}
{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeApplications    #-}
{-# LANGUAGE ViewPatterns    #-}
module Equilibrium.Storage.Equilibrium where

import           Prelude hiding ( lines
                                , unlines
                                , readFile
                                , writeFile
                                )

import           Control.Lens.Indexed           ( imap )
import           Data.Function                  ( (&) )
import           Data.Maybe                     ( isJust , fromMaybe )
import           Data.Text hiding               ( map )
import           Data.Text.IO
import           GHC.Exts                       ( sortWith )

import           ECS

import           Equilibrium.Logic.SimpleId.Types
import           Equilibrium.Logic.SimpleName.Types
import           Equilibrium.Logic.SimpleState.Types

loadingSystem :: LoadingSystem
loadingSystem = imap lineParser <$> lines <$> readFile "todos.txt"

-- | constructs an entity given an id and a string
--lineParser :: Int -> Text -> e
lineParser id line =
            entity [Component_ $ SimpleId id, Component_ $ SimpleState $ isJust name, Component_ $ SimpleName $ fromMaybe line name]
                where name = stripPrefix "x " line

savingSystem :: SavingSystem ()
savingSystem = writeFile "todos.txt" <$> unlines <$> map entitySerializer

-- | converts an entity to a corresponding string representation
entitySerializer entity =
                let (SimpleState state) = get @SimpleState entity
                    (SimpleName  name)  = get @SimpleName  entity
                in (if state then "x " else "") <> name
