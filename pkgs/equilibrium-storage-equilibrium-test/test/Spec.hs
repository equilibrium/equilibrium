import           Test.Hspec
import           Test.QuickCheck

import qualified Test.Equilibrium.Storage.Equilibrium

main :: IO ()
main = hspec $ do
  Test.Equilibrium.Storage.Equilibrium.tests
