{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TemplateHaskell       #-}
module Test.Equilibrium.Storage.Equilibrium where

import           Test.Hspec
import           Test.QuickCheck

import           ECS

import           Control.Lens.Indexed           ( imap )

import           Data.Text                      ( pack )

import           Equilibrium
import           Equilibrium.Logic.SimpleId.Types
import           Equilibrium.Logic.SimpleName.Types
import           Equilibrium.Logic.SimpleState.Types
import           Equilibrium.Storage.Equilibrium

tests = do
    describe "lineParser" $ do
        it "should convert lines with line numbers to entities" $ do
            -- given
            let fileContents =
                    [ pack "first todo"
                    , pack "second todo"
                    , pack "x third todo"
                    ]

            -- when
            let entities =
                    imap lineParser fileContents

            -- then
            length entities `shouldBe` 3
            entities `shouldContain`
                    [ entity [Component_ $ SimpleId 0, Component_ $ SimpleState False, Component_ $ SimpleName "first todo"]
                    , entity [Component_ $ SimpleId 1, Component_ $ SimpleState False, Component_ $ SimpleName "second todo"]
                    , entity [Component_ $ SimpleId 2, Component_ $ SimpleState True,  Component_ $ SimpleName "third todo"]
                    ]

    describe "entitySerializer" $ do
        it "should serialize entities to Text" $ do
            -- given
            let entities =
                    [ entity [Component_ $ SimpleId 1, Component_ $ SimpleState False, Component_ $ SimpleName $ pack "first todo"]
                    , entity [Component_ $ SimpleId 2, Component_ $ SimpleState False, Component_ $ SimpleName $ pack "second todo"]
                    , entity [Component_ $ SimpleId 3, Component_ $ SimpleState True,  Component_ $ SimpleName $ pack "third todo"]
                    ]

            -- when
            let serializedEntities =
                    map entitySerializer entities

            -- then
            length serializedEntities `shouldBe` 3
            serializedEntities `shouldContain`
                    [ pack "first todo"
                    , pack "second todo"
                    , pack "x third todo"
                    ]
