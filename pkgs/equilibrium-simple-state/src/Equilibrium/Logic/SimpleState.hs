{-# LANGUAGE RankNTypes       #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE ScopedTypeVariables #-}
module Equilibrium.Logic.SimpleState where

import           ECS

import           Equilibrium.Logic.SimpleId.Types
import           Equilibrium.Logic.SimpleState.Types

toggleSystem
  :: forall e i
   . ( HasComponent e SimpleState
     , HasComponent e i
     , Component i
     , Eq i
     , RetrievesComponents e
     , SavesComponents e e
     )
  => i -> UpdateSystem e e
toggleSystem targetId entity =
  if get @i entity == targetId
    then
      let
        (SimpleState state) = get @SimpleState entity
      in
      Update $ set (SimpleState (not state)) entity
    else Keep

