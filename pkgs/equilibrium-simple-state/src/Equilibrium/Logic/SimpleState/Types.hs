{-# LANGUAGE OverloadedStrings #-}
module Equilibrium.Logic.SimpleState.Types where

import           Data.Aeson
import           ECS

newtype SimpleState = SimpleState Bool
  deriving (Show)
instance Component SimpleState where
  identifier = "SimpleState"
  serialize (SimpleState state) = Bool state
  deserialize (Bool state) = Just $ SimpleState state
