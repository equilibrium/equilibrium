module Equilibrium.Storage.GitBug
  (gitBugLoadingSystem
  , gitBugSavingSystem
  , GitBugId
  , gitBugIdAsString
  ) where

import           ECS

import           Equilibrium.Storage.GitBug.CLI
import           Equilibrium.Storage.GitBug.Types
import           Equilibrium.Storage.GitBug.Composer

gitBugLoadingSystem :: LoadingSystem
gitBugLoadingSystem = gitBugLoadingSystemComposer lsId status title

gitBugSavingSystem :: [e] -> IO ()
gitBugSavingSystem = undefined
