{-# LANGUAGE OverloadedStrings #-}
module Equilibrium.Storage.GitBug.Types
     ( GitBugId
     , gitBugIdAsString
     ) where

import           Equilibrium.Storage.GitBug.Internal
import           Data.Aeson
import           Data.Text
import           ECS

instance Component GitBugId where
  identifier = "GitBugId"
  serialize (GitBugId id) = String $ pack id
  deserialize (String id) = Just $ GitBugId $ unpack id

