module Equilibrium.Storage.GitBug.CLI
     ( LsIdCLI
     , lsId
     , lsId'
     , TitleCLI
     , title
     , title'
     , StatusCLI
     , status
     , status'
     ) where

import           Equilibrium.Storage.GitBug.Internal
import           Data.Function                  ( (&) )
import           System.Exit                    ( ExitCode(ExitSuccess) )
import           System.Process                 ( readProcess
                                                , readProcessWithExitCode
                                                )

type LsIdCLI = IO [GitBugId]
lsId :: LsIdCLI
lsId = lsId' lsIdProc

lsId' :: IO (ExitCode, String, String) -> LsIdCLI
lsId' proc = do
  (code, out, errOut) <- proc
  case code of
    ExitSuccess ->
      out & lines & map gitBugIdFromString & return
    _ -> return []

lsIdProc :: IO (ExitCode, String, String)
lsIdProc = readProcessWithExitCode "git-bug" ["ls-id"] ""

type TitleCLI = GitBugId -> IO (Maybe String)
title :: TitleCLI
title = title' titleProc

title' :: (String -> IO (ExitCode, String, String)) -> TitleCLI
title' proc gitBugId = do
  let id = gitBugIdAsString gitBugId
  (code, out, errOut) <- proc id

  case code of
    ExitSuccess -> return $ Just $ init out
    _           -> return Nothing

titleProc :: String -> IO (ExitCode, String, String)
titleProc id = readProcessWithExitCode "git-bug" ["title", id] ""

type StatusCLI = GitBugId -> IO (Maybe Bool)
status :: StatusCLI
status = status' statusProc

status' :: (String -> IO (ExitCode, String, String)) -> StatusCLI
status' proc gitBugId = do
  let id = gitBugIdAsString gitBugId
  (code, out, errOut) <- proc id

  case code of
    ExitSuccess -> return $ Just $ init out == "closed"
    _           -> return Nothing

statusProc :: String -> IO (ExitCode, String, String)
statusProc id = readProcessWithExitCode "git-bug" ["status", id] ""
