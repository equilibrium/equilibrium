module Equilibrium.Storage.GitBug.Composer where

import           Data.Functor ((<&>))
import           Data.Maybe                     ( catMaybes )
import           Data.Text                      ( pack )


import           ECS

import           Equilibrium.Logic.SimpleName.Types
import           Equilibrium.Logic.SimpleState.Types

import           Equilibrium.Storage.GitBug.CLI
import           Equilibrium.Storage.GitBug.Types

gitBugLoadingSystemComposer :: LsIdCLI -> StatusCLI -> TitleCLI -> LoadingSystem
gitBugLoadingSystemComposer lsId status title =
  let
    completeIssue :: GitBugId -> IO (Maybe (GitBugId, Bool, String))
    completeIssue id = do
      sMaybe <- status id
      tMaybe <- title id
      pure $ do
        s <- sMaybe
        t <- tMaybe
        Just (id, s, t)
  in do
    lsId >>= mapM completeIssue <&> catMaybes <&>
      map
        (\(id, state, name) ->
          entity [Component_ id, Component_ $ SimpleState state, Component_ $ SimpleName $ pack name]
        )
