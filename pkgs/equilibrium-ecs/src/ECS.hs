{-# LANGUAGE ExistentialQuantification #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE AllowAmbiguousTypes   #-}
{-# LANGUAGE TypeApplications      #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE TypeSynonymInstances  #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE RankNTypes            #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE ImplicitParams        #-}
{-# LANGUAGE LambdaCase        #-}
module ECS
  ( Component
  , identifier
  , serialize
  , deserialize
  , HasComponent
  , RetrievesComponents
  , SavesComponents
  , LoadingSystem
  , UpdateResult(..)
  , UpdateSystem
  , SavingSystem
  , getMaybe
  , get
  , set
  , applySystem
  , entity
  , Component_(Component_)
  ) where

import           Control.DeepSeq
import           Control.Exception
import           Data.Aeson
import           Data.HashMap.Lazy       hiding ( map
                                                , mapMaybe
                                                )
import           Data.Maybe
import           Data.Text               hiding ( head
                                                , map
                                                )

-- CLASSES

class Component c where
  identifier :: Text

  identifier' :: c -> Text
  identifier' _ = identifier @c

  serialize :: c -> Value
  deserialize :: Value -> Maybe c

data Component_ = forall c . Component c => Component_ c
instance Component Component_ where
  identifier' (Component_ c) = identifier' c
  serialize (Component_ c) = serialize c
  deserialize v = deserialize v

class HasComponent e c

class RetrievesComponents e where
  getComponent :: e -> Text -> Maybe Value

class SavesComponents e e' where
  insertComponent :: Text -> Value -> e -> e'

-- TYPES AND INSTANCES

type Entity = HashMap Text Value

instance RetrievesComponents Entity where
  getComponent = (!?)

instance SavesComponents Entity Entity where
  insertComponent = insert

instance HasComponent Entity c

data UpdateResult e' = Delete | Keep | Update e'

type UpdateSystem e e'
  = (SavesComponents e e', ?entities::[Entity]) => e -> UpdateResult e'

type LoadingSystem = IO [Entity]

type SavingSystem a = [Entity] -> IO a

type GetInfoSystem a = (?entities::[Entity]) => Entity -> a

-- FUNCTIONS

getMaybe :: forall c e . (Component c, RetrievesComponents e) => e -> Maybe c
getMaybe entity = case getComponent entity (identifier @c) of
  Nothing    -> Nothing
  Just value -> deserialize @c value

get
  :: forall c e
   . (Component c, RetrievesComponents e, HasComponent e c)
  => e
  -> c
get entity =
  fromJust $ deserialize @c =<< getComponent entity (identifier @c)

set
  :: forall c e e'
   . (Component c, SavesComponents e e', HasComponent e' c)
  => c
  -> e
  -> e'
set component =
  insertComponent (identifier @c) $ serialize @c component

applySystem :: UpdateSystem Entity Entity -> [Entity] -> IO [Entity]
applySystem updateSystem entities = catMaybes <$> mapM updateSystem' entities
 where
  updateSystem' :: Entity -> IO (Maybe Entity)
  updateSystem' entity =
    let ?entities = entities
    in  catch (evaluate . force $ (\case
            Delete -> Nothing
            Keep -> Just entity
            Update e -> Just e) $ updateSystem entity)
          $ \(_ :: SomeException) -> pure $ Just entity

entity :: [Component_] -> Entity
entity components = fromList $ map
  (\component -> (identifier' component, serialize component))
  components

