{-# LANGUAGE RankNTypes            #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE KindSignatures        #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE TemplateHaskell       #-}
module DefaultMain where


import           Data.List                      ( intercalate )
import           Data.Text                      ( Text
                                                , pack
                                                , unpack
                                                )
import qualified Data.Text                     as D
import qualified Data.Text.IO                  as T
import           System.Environment             ( getArgs )
import           System.Exit                    ( die )
import           Text.Read                      ( readMaybe )

import           ECS

import           Equilibrium
import           Equilibrium.Logic.SimpleId.Types
import           Equilibrium.Logic.SimpleInterface
import           Equilibrium.Logic.SimpleName
import           Equilibrium.Logic.SimpleName.Types
import           Equilibrium.Logic.SimpleState
import           Equilibrium.Logic.SimpleState.Types
import           Equilibrium.Logic.TaskellState.Types
import           Equilibrium.Storage.Equilibrium
import qualified Equilibrium.Storage.GitBug    as GitBug
import           Equilibrium.Storage.GitBug.Types
import qualified Equilibrium.Storage.Taskell   as TS

main :: IO ()
main = putStrLn "TODOREF" -- getArgs >>= executeCommand

-- TODOREF
-- displayTaskell
--   :: forall w
--    . (Has w IO SimpleName, Has w IO TaskellColumnState)
--   => SystemT w IO Text
-- displayTaskell = do
--   todosList <- cfold
--     (\ls (SimpleName name, TaskellColumnState state) ->
--       (D.concat [(pack state), ": ", name]) : ls
--     )
--     []
--   return $ D.intercalate "\n" todosList
-- 
-- listTasksSystem
--   :: forall w
--    . ( Has w IO GitBugId
--      , Has w IO SimpleId
--      , Has w IO SimpleState
--      , Has w IO SimpleName
--      )
--   => SystemT w IO String
-- listTasksSystem = do
--   todosList <- cfold
--     (\ls (id :: GitBugId, SimpleState state, SimpleName name) ->
--       (  (if state then "DONE" else "TODO")
--         ++ " "
--         ++ show id
--         ++ " "
--         ++ (unpack name)
--         )
--         : ls
--     )
--     []
-- 
--   return $ intercalate "\n" todosList
-- 
-- executeCommand :: [String] -> IO ()
-- executeCommand args = case args of
--   []                  -> die "What should I do?"
--   "toggle"       : [] -> die "Please pass a number"
--   "toggle" : num : _  -> case readMaybe num :: Maybe Int of
--     Nothing       -> die "Please pass a valid number"
--     Just targetId -> initTaskManager >>= runSystem
--       (updateSystem (loadingSystem loadingIOSystem loadingParseSystem)
--                     (toggleSystem targetId)
--                     savingSystem
--       )
--   "rename"       : []      -> die "Please provide an id and new name"
--   "rename" : num : newName -> case readMaybe num :: Maybe Int of
--     Nothing       -> die "Please pass a valid number for id"
--     Just targetId -> case newName of
--       [] -> die "Please pass a valid name"
--       _  -> do
--         let newName' = unwords newName
--         initTaskManager >>= runSystem
--           (updateSystem (loadingSystem loadingIOSystem loadingParseSystem)
--                         (renameSystem (SimpleId targetId) newName')
--                         savingSystem
--           )
--   "add" : []   -> die "Please provide a name"
--   "add" : name -> do
--     let name' = unwords name
--     initTaskManager >>= runSystem
--       (updateSystem (loadingSystem loadingIOSystem loadingParseSystem)
--                     (addTodoSystem (SimpleId (-1)) name')
--                     savingSystem
--       )
--   "ls" : args' -> executeCommand $ "list" : args'
--   "list" : _ ->
--     initTaskManager
--       >>= runSystem
--             (getInfoSystem (loadingSystem loadingIOSystem loadingParseSystem)
--                            simpleListTasksSystem
--             )
--       >>= putStrLn
--   "git-bug-ls" : _ ->
--     initTaskManager
--       >>= runSystem (getInfoSystem GitBug.loadingSystem listTasksSystem)
--       >>= putStrLn
--   "delete"       : [] -> die "Please provide an id"
--   "delete" : num : _  -> case readMaybe num :: Maybe Int of
--     Nothing       -> die "Please pass valid number"
--     Just targetId -> initTaskManager >>= runSystem
--       (updateSystem (loadingSystem loadingIOSystem loadingParseSystem)
--                     (deleteTodoSystem (SimpleId targetId))
--                     savingSystem
--       )
--   "taskell-ls" : _ ->
--     initTaskManager
--       >>= runSystem (getInfoSystem TS.loadingSystem displayTaskell)
--       >>= T.putStrLn
--   _ -> die "Unknown command"
-- 
