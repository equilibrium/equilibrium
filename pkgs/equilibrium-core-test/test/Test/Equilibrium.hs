{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TemplateHaskell       #-}
module Test.Equilibrium where

import           Test.Hspec
import           Test.QuickCheck

import           Data.Text                      ( Text
                                                , unpack
                                                )
import           ECS
import           GHC.Exts                       ( sortWith )

import           Equilibrium
import           Equilibrium.Logic.SimpleId.Types
import           Equilibrium.Logic.SimpleName.Types
import           Equilibrium.Logic.SimpleState.Types

-- TODOREF
-- generateTaskManagerWithComponents [ ''SimpleId, ''SimpleState, ''SimpleName ]
-- 
-- tests = do
--   describe "updateSystem" $ do
--     it "should update TODOs" $ do
--       -- given
--       taskMgr <- initTaskManager
-- 
--       let loadingSystem :: System TaskManager (IO ())
--           loadingSystem = do
--             newEntity (SimpleId 0, SimpleState False, SimpleName "jey")
--             return $ liftIO $ return ()
-- 
--       let
--         logicSystem = do
--           cmap $ \(SimpleId id, SimpleState state) -> (SimpleState $ not state)
--           return ()
-- 
--       let savingSystem = do
--             newEntity (SimpleId 2, SimpleState True, SimpleName "j0")
--             return $ liftIO $ return ()
-- 
--       -- when
--       runSystem (updateSystem loadingSystem logicSystem savingSystem) taskMgr
-- 
--       -- then
--       toggledTodos <- runSystem
--         (cfold
--           (\ts (SimpleId id, SimpleState s, SimpleName n) -> (id, s, n) : ts)
--           []
--         )
--         taskMgr
-- 
--       length toggledTodos `shouldBe` 2
--       toggledTodos `shouldContain` [(0, True, "jey")]
--       toggledTodos `shouldContain` [(2, True, "j0")]
-- 
--   describe "getInfoSystem" $ do
--     it "should extract information" $ do
--       taskMgr <- initTaskManager
-- 
--       let loadingSystem :: System TaskManager (IO ())
--           loadingSystem = do
--             newEntity (SimpleId 0, SimpleState False, SimpleName "jey")
--             return $ liftIO $ return ()
-- 
--       let extractInfoSystem :: System TaskManager String
--           extractInfoSystem = do
--             todos <- cfold
--               (\ts (SimpleId id, SimpleState state, SimpleName name) ->
--                 (id, state, name) : ts
--               )
--               []
-- 
--             let fileContents =
--                   unlines
--                     $ map
--                         (\(_, isDone, name) ->
--                           '['
--                             :  (if isDone then 'X' else ' ')
--                             :  "] "
--                             ++ (unpack name)
--                         )
--                     $ sortWith (\(id, _, _) -> id) todos
--             return fileContents
-- 
--       fileContents <- runSystem
--         (getInfoSystem loadingSystem extractInfoSystem)
--         taskMgr
-- 
--      fileContents `shouldBe` unlines ["[ ] jey"]
