.PHONY: clean build default.nix all test shell pkg dev hls hls-setup
.NOTPARALLEL: all

all: clean hls-setup build

all-push: clean hls-setup build-push

clean:
	sh scripts/clean.sh

build: generated
	nix-build default.nix

build-push: generated
	nix-build | cachix push code-done

dev:
	find pkgs -type f -name '*.hs' -o -name '*.cabal' | entr -dcr make test

generated/test.nix:
	mkdir -p generated
	sh scripts/generate-test.nix.sh > generated/test.nix

generated/generatedOverrides.nix:
	mkdir -p generated
	sh scripts/generate-haskellPackageOverrides.sh > generated/generatedOverrides.nix

default.nix:
	nix-shell --pure --run "./scripts/generate-default.nix-expressions.sh" ./nix/setupshell.nix

default.nix-noshell:
	./scripts/generate-default.nix-expressions.sh

generated: generated/test.nix generated/generatedOverrides.nix default.nix

generated-noshell: generated/test.nix generated/generatedOverrides.nix default.nix-noshell

test: generated
	nix-build ./generated/test.nix
	./result/bin/test

shell: generated
	nix-shell shell.nix

pkg:
	sh scripts/new-package.sh


hls:
	nix-shell --run 'haskell-language-server-wrapper --lsp'

# Update the cabal package database and generates hie.yaml files in each package for the haskell language server
hls-setup: generated
	nix-shell --run 'scripts/generate-hie.sh && cabal update'
