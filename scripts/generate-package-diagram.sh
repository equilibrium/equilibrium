#!/bin/sh

# output file without suffix
result='generated/packages'

# comment this line to include tests
exclude='\-test'

# uncomment to label the arrows
# label=' : <<import>>'

# removes dashes from package names since plantuml can't work with them
pformat() {
  sed 's/equilibrium-/equilibrium./g;s/-test/.test/g;s/-/_/g'
}

mkdir -p "$(dirname $result)"
umlfile="$result.plantuml"

(
echo '@startuml'
output=$(find pkgs -name '*.cabal' | grep -v "${exclude:-___}" | while read f; do
  pkg=$(basename ${f//.cabal} | pformat)
  echo "package $pkg {}"
  cat $f | sed ':a;N;$!ba;s/\n/ /g' | grep -o -P '(?<=build-depends: )[^:]*(?= [a-z-]*:)' | sed 's/ *, /\n/g' | grep 'equilibrium' | pformat | sed "s/\([^ ]*\) */$pkg --> \1$label/" | sed '1i\ '
done)
echo "$output"
echo "@enduml"
) >"$umlfile" # for debugging: | tee "$umlfile"

plantuml "$umlfile"

test "$1" = "-o" && xdg-open "$result.png"
