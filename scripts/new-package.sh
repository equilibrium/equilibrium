#!/usr/bin/env sh

set -e

echo "Is it a test-package?"
while true; do
  read -p "  [y/n] " yn
  case $yn in
    [Yy]* ) read -p "What should the test-package be called? (without -test suffix)? " testname;\
      pkgname="equilibrium-$testname-test";
      isTest=true;
      break;;
    [Nn]* ) read -p "What should the package be called? " pkgname;\
      pkgname="equilibrium-$pkgname";
      isTest=false;
      break;;
    * ) echo "Please answer yes or no.";;
  esac
done

find pkgs -maxdepth 1 -type d -name $pkgname -name "$pkgname" \
  | while read in;
      do echo "A package with the name $pkgname already exists." >&2 && \
      exit 1;
    done;

echo "Creating ./pkgs/$pkgname/..."
mkdir "./pkgs/$pkgname"

echo "Creating ./pkgs/$pkgname/$pkgname.cabal..."
echo "cabal-version: 2.2

name:
  $pkgname
version:
  0.1.0.0
build-type:
  Simple
" > "./pkgs/$pkgname/$pkgname.cabal"
if $isTest; then
  echo "
executable $pkgname
  main-is: Spec.hs
  -- other-modules:
  build-depends: base
               , hspec
               , QuickCheck
               , $testname
  hs-source-dirs: test
  default-language: Haskell2010
" >> "./pkgs/$pkgname/$pkgname.cabal"
else
  echo "
library
  hs-source-dirs: src
  -- exposed-modules:
  build-depends: base
  default-language: Haskell2010
" >> "./pkgs/$pkgname/$pkgname.cabal"
fi

if $isTest; then
  echo "Creating ./pkgs/$pkgname/test/..."
  mkdir "./pkgs/$pkgname/test/"

  echo "Creating ./pkgs/$pkgname/test/Spec.hs"
  echo "import           Test.Hspec
import           Test.QuickCheck

main :: IO ()
main = hspec $ do
  undefined
" > "./pkgs/$pkgname/test/Spec.hs"
else
  echo "Creating ./pkgs/$pkgname/src/..."
  mkdir "./pkgs/$pkgname/src/"
fi

echo "🚀 DONE 🚀"

