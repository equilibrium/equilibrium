#!/usr/bin/env sh

rm -rfv ./generated ./result
find pkgs \( -name dist-newstyle -o -name hie.yaml \) -printf "removing '%p'\n" -exec rm -rf {} +

