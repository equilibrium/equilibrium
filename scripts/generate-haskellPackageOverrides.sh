#!/usr/bin/env sh

echo "{"
echo "  haskellPackagesOverrides = self: super: {"

./scripts/list-packages.sh \
  | while read in; do echo "    $in = self.callPackage ./pkg-$in.nix {};"; done

echo "  };"
echo "}"
