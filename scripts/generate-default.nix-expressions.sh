#!/usr/bin/env sh

mkdir -p ./generated
./scripts/list-packages.sh \
  | while read in;
      do outPath="./generated/pkg-$in.nix";\
         echo "Generating ./generated/pkg-$in.nix" && \
            echo "let
  gitignoreSource = (import (import ../nix/sources.nix).gitignore {}).gitignoreSource;
in" > "$outPath" && \
            cabal2nix "./pkgs/$in" \
              | sed -r 's/src = .\/(.*);/src = gitignoreSource ..\/\1;/g' \
                >> "$outPath"; \
    done

