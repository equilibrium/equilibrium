set -e
find pkgs/ -name "*.cabal" -printf "%h\n" | while read cab;
	do cd "$cab"
	echo "Generating $cab/hie.yaml"
	gen-hie >hie.yaml
	cd "$OLDPWD"
done
