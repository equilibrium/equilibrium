#!/usr/bin/env sh

echo "let
  sources = import ../nix/sources.nix;
  gitignoreSource = (import sources.gitignore {}).gitignoreSource;
  pkgs = import ../nix/packages.nix;
in
  pkgs.stdenv.mkDerivation {
    name = \"test\";

    src = gitignoreSource ../.;

    buildPhase = with pkgs.haskellPackages; ''
      mkdir -p ./bin
      echo \"#!\${pkgs.stdenv.shell}\" > ./bin/test
      echo \"set -e\" >> ./bin/test
"

./scripts/list-test-packages.sh \
  | while read in; do echo "      echo \"\${$in}/bin/$in\" >> ./bin/test"; done

echo "
      echo \"echo \\\"All good!\\\"\" >> ./bin/test
      chmod +x ./bin/test
    '';

    installPhase = ''
      mkdir -p \$out/bin
      cp ./bin/test \$out/bin/
    '';

    buildInputs = with pkgs.haskellPackages; ["

./scripts/list-test-packages.sh \
  | while read in; do echo "      $in"; done

echo "    ];
  }"
