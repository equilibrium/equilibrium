#!/usr/bin/env sh

find pkgs -type f -name \*.cabal \
  | sed -r 's/.*\/(.*)\.cabal/\1/g' \
  | sort
