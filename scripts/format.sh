#!/usr/bin/env sh

brittany --write-mode=inplace $(printf "%s\n%s\n" \
    "$(find pkgs -type f -name \*.hs)" \
    "$(find pkgs -type f -name \*.cabal)" \
)
