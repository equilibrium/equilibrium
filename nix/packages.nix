let
  config = import ../config.nix;
  sources = import ./sources.nix;
  pkgs = import sources.nixpkgs {};

  generatedOverrides = import ../generated/generatedOverrides.nix;

  haskellPackages = pkgs.haskell.packages.${config.compiler}.override {
    overrides = generatedOverrides.haskellPackagesOverrides;
  };
in
  pkgs // { haskellPackages = haskellPackages // {
    main = config.getMainPackage haskellPackages;
  }; }

