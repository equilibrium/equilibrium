let
  pkgs = import ./packages.nix;
in
  pkgs.mkShell {
    buildInputs = [
      pkgs.gnumake
      pkgs.cabal2nix
    ];
  }
