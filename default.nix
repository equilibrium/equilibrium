let
  sources = import ./nix/sources.nix;
  gitignoreSource = (import sources.gitignore {}).gitignoreSource;
  pkgs = import ./nix/packages.nix;
  pkg = (import ./nix/packages.nix).haskellPackages.main;
  test = import ./generated/test.nix;
  config = import ./config.nix;
in
  pkgs.stdenv.mkDerivation {
    name = config.name;

    src = gitignoreSource ./.;

    buildPhase = ''
      ${test}/bin/test
    '';

    installPhase = ''
      cp -r ${pkg} $out
    '';

    buildInputs = [ pkg test ];
  }
