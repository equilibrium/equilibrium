let
  pkgs = import ./nix/packages.nix;
  inherit (pkgs) haskellPackages;

  project = pkgs.haskellPackages.main;

  config = import ./config.nix;
  shellProvidedPackages = config.shellProvidedPackages pkgs;
in
  pkgs.mkShell {
    buildInputs = project.env.nativeBuildInputs ++ shellProvidedPackages ++ [
      pkgs.haskellPackages.cabal-install
      pkgs.haskellPackages.ghc
      pkgs.haskellPackages.haskell-language-server
    ];
  }
